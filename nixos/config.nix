{ config, pkgs, lib, flakeInputs, deviceName, ... }:

{
  # Hardware Configuration
  # ======================
  imports = [ (./devices + "/${deviceName}.nix") ];

  # Examples
  # --------
  # # GRUB:
  # boot.loader.grub.device = "/dev/sda";
  #
  # Or:
  #
  # # Systemd-boot EFI boot loader:
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  # Wireless
  # ========
  # networking.hostName = "nixos";
  # networking.wireless.enable = true;

  # Internationalisation
  # ====================
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";
  i18n.defaultLocale = "en_US.UTF-8";

  # Time Zone
  # =========
  time.timeZone = "Australia/Sydney";

  # Nix Packages
  # ============
  # nix.nixPath = [
  #   "nixos-config=/home/u/dots/nixos/config.nix"
  #   "nixpkgs=/home/u/nixpkgs"
  # ];

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
  nix.package = pkgs.nixUnstable;
  # Pin nixpkgs to the version that built the system so that for example, `nix
  # shell nixpkgs#<package>` will likely work more efficiently.
  nix.registry.nixpkgs.flake = flakeInputs.nixpkgs;
  nix.settings.sandbox = true;

  nixpkgs.overlays = [
    (import ./overlay.nix)
  ];

  # Fonts
  # -----
  fonts.packages = with pkgs; [
    # Might be from overlays.
    eb-garamond
    # eb-garamond12
    # inconsolata
    iosevka
    # iosevka-custom
    # lato
    noto-fonts
    noto-fonts-emoji
  ];

  # Shell
  # -----
  # programs.zsh.enable = true;
  # programs.zsh.syntaxHighlighting.enable = true;
  # programs.zsh.syntaxHighlighting.highlighters = [ "main" "brackets" ];

  # Packages
  # --------
  environment.systemPackages = with pkgs; [
    acpi
    alacritty
    aria
    # bibutils
    # blesh
    # chatterino2
    # chromium
    # compton
    curl
    # deer
    # emacs
    entr
    evtest
    fd
    ffmpeg
    file
    firefox
    foot
    git
    ghc
    gnumake
    gnupg
    grim
    # guile
    # herbstluftwm
    # heroku
    htop
    # idris
    imagemagick
    imv
    # irssi
    # janet
    # libwebp
    lm_sensors
    # maim
    mediainfo
    megatools
    # mplayer
    mpv
    # mytexlive
    # next
    nix-prefetch-github
    # nixUnstable
    # nodejs
    nodePackages.node2nix
    oh
    ormolu
    p7zip # abandoned
    # poetry
    # polybar
    # pqiv
    # purescript
    # racket
    renpy
    # scrot
    # setroot
    slurp
    # stack
    stow
    # streamlink
    # swaybg
    # sxhkd
    sxiv
    syncthing
    # termite
    tiny
    tree
    # ttfautohint
    udiskie
    udisks
    unar
    # ungoogled-chromium
    # unrpa
    unzip
    vimHugeX
    # FIXME: These plugins do not seem to load.
    vimPlugins.vim-commentary
    vimPlugins.vim-repeat
    vimPlugins.vim-surround
    wget
    wl-clipboard
    # xlsfonts
    # xorg.mkfontdir
    # xorg.mkfontscale
    # xorg.xbacklight
    # xorg.xev
    # xorg.xkill
    # xorg.xrdb
    # xorg.xset
    # xorg.xsetroot
    # xsel
    # xst
    # xvkbd
    # xwinwrap
    # yarn
    youtube-dl
    zathura
    # zig
  ];

  # Services
  # ========
  services.openssh.enable = true;
  programs.ssh.askPassword = "";
  services.printing.enable = true;
  services.udisks2.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Interception Tools
  # ------------------
  services.interception-tools.enable = true;
  services.interception-tools.plugins =
    [ pkgs.interception-tools-plugins.personal ];
  services.interception-tools.udevmonConfig =
    ../interception-tools/udevmon.yaml;

  # X Windows
  # ---------
  # services.xserver.enable = true;
  # services.xserver.layout = "us";

  # Display Manager
  # ---------------
  # services.xserver.displayManager.startx.enable = true;

  # Put in a separate file so you don't have to do `nixos-rebuild' everytime
  # you change the session commands.
  # services.xserver.displayManager.sessionCommands = ". ~/dots/x/init";
  # services.xserver.displayManager.slim = {
  #   enable = true;
  #   # Removes ugly "Session: [...]" text by placing it out of screen.
  #   extraConfig = "session_y 110%";
  #   theme = pkgs.fetchFromGitHub {
  #     owner = "naglis";
  #     repo = "slim-minimal";
  #     rev = "65759e026e8de1f957889e81ca6faf3b8c2167a7";
  #     sha256 = "0ggkxgx5bdf3yvgfhs594v1h6nkjq6df4kfg5d51jpga0989c28y";
  #   };
  # };

  # Touchpad
  # --------
  # services.xserver.synaptics = {
  #   enable = true;
  #   twoFingerScroll = true;
  #   horizontalScroll = true;
  #   horizTwoFingerScroll = true;
  #   vertTwoFingerScroll = true;
  # };

  # Window Manager
  # --------------
  # services.xserver.windowManager.herbstluftwm.enable = true;
  programs.sway.enable = true;

  # Users
  # =====
  users.defaultUserShell = pkgs.bashInteractive;
  users.users.u = {
    extraGroups = [ "wheel" ];
    isNormalUser = true;
    uid = 1000;
  };

  # Guix
  # ====

  # Guix is not packaged for NixOS, due to reasons, see [1].
  #
  # The following then is a custom combination of following manual
  # (non-declarative) binary installation instructions [2] and some NixOS
  # options to handle Guix daemon users and systemd services. This was for
  # installing Guix 1.3.0, refer to the latest version of [2] to see if the
  # installation process has changed for the version of Guix you are installing.
  #
  # If you are using this NixOS configuration for installation of another NixOS
  # system, you should (probably) comment out the following options, and install
  # NixOS on its own first before attempting to install Guix on NixOS.
  #
  # Short version of installation instructions is as follows, (see [2] for more
  # details):
  #
  # Requirements: wget (or curl), gnupg, GNU tar and xz (and sudo?).
  #
  # 1. $ wget 'https://ftp.gnu.org/gnu/guix/guix-binary-1.3.0.x86_64-linux.tar.xz'
  #    $ wget 'https://ftp.gnu.org/gnu/guix/guix-binary-1.3.0.x86_64-linux.tar.xz.sig'
  #    $ gpg --verify guix-binary-1.3.0.x86_64-linux.tar.xz.sig
  #
  #    If verification fails:
  #
  #    $ wget 'https://sv.gnu.org/people/viewgpg.php?user_id=127547' -qO - | gpg --import -
  #
  #    And rerun `gpg --verify` command. "This key is not certified with a
  #    trusted signature!" warning is normal.
  #
  # 2. # cd /tmp
  #    # tar --warning=no-timestamp -xf /path/to/guix-binary-1.3.0.x86_64-linux.tar.xz
  #    # mv var/guix /var/ && mv gnu /
  #
  # 3. # mkdir -p ~root/.config/guix
  #    # ln -sf /var/guix/profiles/per-user/root/current-guix ~root/.config/guix/current
  #
  # 4. Uncomment the NixOS options in this section and:
  #
  #    $ sudo nixos-rebuild switch --flake .
  #
  #    from the root of this repository to realize them.
  #
  # 5. # cd ~root
  #    # echo export PATH="`echo ~root`/.config/guix/current/bin:"'$PATH' >> .bash_profile
  #    # source .bash_profile
  #    # guix archive --authorize < ~root/.config/guix/current/share/guix/ci.guix.gnu.org.pub
  #    # guix pull [-C /path/to/channels.scm]
  #    # guix install glibc-locales
  #    # echo GUIX_PROFILE="`echo ~root`/.guix-profile" >> .bash_profile
  #    # echo source "$GUIX_PROFILE/etc/profile" >> .bash_profile
  #
  #    PS. If ever installing more packages in root profile declaratively,
  #    remember to include glibc-locales in the manifest.
  #
  # 6. Make `guix' command available to other users for example by the following
  #    in a user's ~/.bash_profile:
  #
  #        export PATH="/var/guix/profiles/per-user/root/current-guix/bin:$PATH"
  #
  #    Or see ../bash/dot-bash_profile.
  #
  # 7. [3]
  #
  # [1]: https://github.com/NixOS/nixpkgs/pull/150130
  # [2]: https://guix.gnu.org/en/manual/en/guix.html#Binary-Installation
  # [3]: https://guix.gnu.org/en/manual/en/guix.html#Application-Setup

  users.extraUsers = lib.fold (a: b: a // b) { } (builtins.map
    (i: {
      "guixbuilder${i}" = {
        group = "guixbuild";
        extraGroups = [ "guixbuild" ];
        home = "/var/empty";
        # pkgs.nologin renamed/moved to pkgs.shadow
        shell = pkgs.shadow;
        description = "Guix build user ${i}";
        isSystemUser = true;
      };
    }) [ "01" "02" "03" "04" "05" "06" "07" "08" "09" "10" ]);
  users.extraGroups.guixbuild = { name = "guixbuild"; };

  systemd.services.guix-daemon = {
    enable = true;
    description = "Build daemon for GNU Guix";
    serviceConfig = {
      ExecStart = "/var/guix/profiles/per-user/root/current-guix/bin/guix-daemon --build-users-group=guixbuild";
      Environment = [
        "GUIX_LOCPATH=/var/guix/profiles/per-user/root/guix-profile/lib/locale"
        "LC_ALL=en_US.utf8"
      ];
      RemainAfterExit = "yes";
      StandardOutput = "syslog";
      StandardError = "syslog";

      # See <https://lists.gnu.org/archive/html/guix-devel/2016-04/msg00608.html>.
      # Some package builds (for example, go@1.8.1) may require even more than
      # 1024 tasks.
      TasksMax = "8192";
    };
    wantedBy = [ "multi-user.target" ];
  };

  systemd.mounts = [{
    what = "/gnu/store";
    where = "/gnu/store";
    description = "Read-only /gnu/store for GNU Guix";
    unitConfig = {
      DefaultDependencies = "no";
      ConditionPathExists = "/gnu/store";
      Before = "guix-daemon.service";
    };
    wantedBy = [ "guix-daemon.service" ];
    mountConfig = {
      Type = "none";
      Options = "bind,ro";
    };
  }];
}
