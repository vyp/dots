# Basic Settings
# ==============

bleopt input_encoding=UTF-8
bleopt pager=less
bleopt editor=vim
bleopt vbell_default_message='[ !!! ]'
bleopt vbell_duration=2000
bleopt vbell_align=right

# Line Editor Settings
# ====================

# https://github.com/akinomyoga/ble.sh/wiki/Recipes#r4-show-current-working-directory-and-git-branch-in-right-prompt
_blerc_prompt_git_vars=(git_base)

function blerc/prompt/git/initialize {
  type git &>/dev/null || return 1
  local path=$PWD

  while
    if [[ -f $path/.git/HEAD ]]; then
      git_base=$path
      return 0
    fi
    [[ $path == */* ]]
  do path=${path%/*}; done

  return 1
}

function blerc/prompt/git/get-head-information {
  branch= hash=

  local head_file=$git_base/.git/HEAD
  [[ -s $head_file ]] || return 1
  local content; ble/util/mapfile content < "$head_file"

  if [[ $content == *'ref: refs/heads/'* ]]; then
    branch=${content#*refs/heads/}

    local branch_file=$git_base/.git/refs/heads/$branch
    [[ -s $branch_file ]] || return 1
    local content; ble/util/mapfile content < "$branch_file"
  fi

  [[ ! ${content//[0-9a-fA-F]} ]] && hash=$content
  return 0
}

function blerc/prompt/git/get-tag-name {
  tag=
  local hash=$1; [[ $hash ]] || return 1

  local file tagsdir=$git_base/.git/refs/tags hash1
  local files ret; ble/util/eval-pathname-expansion '"$tagsdir"/*'; files=("${ret[@]}")

  for file in "${files[@]}"; do
    local tag1=${file#$tagsdir/}
    [[ -s $file ]] || continue
    ble/util/mapfile hash1 < "$file"

    if [[ $hash1 == "$hash" ]]; then
      tag=$tag1
      return 0
    fi
  done
}

function blerc/prompt/git/describe-head {
  ret=

  local hash branch
  blerc/prompt/git/get-head-information

  if [[ $branch ]]; then
    local sgr=$'\e[35m' sgr0=$'\e[m' sgr3=$'\e[33m'
    ret=$sgr$branch$sgr0
    [[ $hash ]] && ret="$ret $sgr3${hash::7}$sgr0"
    return 0
  fi

  local DETACHED=$'\e[91mDETACHED\e[m'

  local tag
  blerc/prompt/git/get-tag-name "$hash"

  if [[ $tag ]]; then
    local sgr=$'\e[35m' sgr0=$'\e[m' sgr3=$'\e[33m'
    ret=$sgr$tag$sgr0
    [[ $hash ]] && ret="$ret $sgr3${hash::7}$sgr0"
    ret="$DETACHED $ret"
    return 0
  fi

  if [[ $hash ]]; then
    local sgr0=$'\e[m' sgr3=$'\e[33m'
    ret="$DETACHED $sgr3${hash::7}$sgr0"
    return 0
  fi

  ret=$'\e[91mUNKNOWN\e[m'
}

function ble/prompt/backslash:X {
  local "${_blerc_prompt_git_vars[@]/%/=}"
  local sgr=$'\e[34m' sgr0=$'\e[m' sgr6=$'\e[36m'

  if blerc/prompt/git/initialize; then
    local name=$sgr${git_base##*?/}$sgr0
    local ret; blerc/prompt/git/describe-head; local branch=$ret
    ble/prompt/print "$name $branch "
    [[ $PWD == "$git_base"/?* ]] && ble/prompt/print "$sgr6./${PWD#$git_base/}$sgr0 "
  else
    ble/prompt/process-prompt-string $sgr'\w '$sgr0
  fi

  return 0
}

# Audible/visible bells.
bleopt edit_abell=
bleopt edit_vbell=1

bleopt history_lazyload=1
bleopt delete_selection_mode=
bleopt indent_offset=2
bleopt indent_tabs=0
bleopt undo_point=end
bleopt edit_forced_textmap=1
bleopt edit_line_type=logical
bleopt info_display=top
bleopt prompt_ps1_final=
bleopt prompt_ps1_transient=trim
bleopt prompt_rps1='\X'
bleopt prompt_rps1_final=''
bleopt prompt_rps1_transient=1
bleopt prompt_xterm_title=
bleopt prompt_screen_title=
bleopt prompt_term_status=
bleopt prompt_status_line=
bleopt prompt_status_align=left
bleopt prompt_eol_mark=$'\e[94m[ble: EOF]\e[m'
bleopt prompt_ruler=
bleopt prompt_command_changes_layout=
bleopt exec_errexit_mark=$'\e[91m[ble: exit %d]\e[m'
bleopt exec_elapsed_mark=$'\e[94m[ble: elapsed %s (CPU %s%%)]\e[m'
bleopt exec_elapsed_enabled='usr+sys>=10000'
bleopt allow_exit_with_jobs=
bleopt history_preserve_point=
bleopt history_share=
bleopt history_erasedups_limit=
bleopt accept_line_threshold=5
bleopt line_limit_type=editor
bleopt line_limit_length=10000
bleopt history_limit_length=10000

# Completion Settings
# -------------------

bleopt complete_auto_complete=
bleopt complete_menu_complete=1
bleopt complete_menu_filter=1
bleopt complete_ambiguous=1
bleopt complete_contract_function_names=1
bleopt complete_allow_reduction=
bleopt complete_auto_history=
bleopt complete_auto_delay=100
bleopt complete_auto_wordbreaks=$' \t\n'
bleopt complete_auto_menu=
bleopt complete_polling_cycle=100
bleopt complete_limit=500
bleopt complete_limit_auto=200
bleopt complete_limit_auto_menu=100
bleopt complete_timeout_auto=2500
bleopt complete_timeout_compvar=200
bleopt complete_menu_style=align-nowrap
bleopt complete_skip_matched=on
bleopt complete_menu_color=on
bleopt complete_menu_color_match=on
bleopt menu_align_min=4
bleopt menu_align_max=20
bleopt complete_menu_maxlines=10
bleopt menu_linewise_prefix=
bleopt menu_desc_multicolumn_width=65

bind 'set completion-ignore-case on'
bind 'set visible-stats on'
bind 'set mark-directories on'
bind 'set mark-symlinked-directories off'
bind 'set match-hidden-files off'
bind 'set menu-complete-display-prefix off'

# Terminal State Control
# ======================

bleopt term_cursor_external=0
bleopt term_modifyOtherKeys_external=auto
bleopt term_modifyOtherKeys_internal=auto

# User Input Settings
# ===================

bleopt default_keymap=vi
bleopt decode_isolated_esc=esc
# 28 = C-\
bleopt decode_abort_char=28
bleopt decode_error_char_abell=
bleopt decode_error_char_vbell=1
bleopt decode_error_char_discard=
bleopt decode_error_cseq_abell=
bleopt decode_error_cseq_vbell=1
bleopt decode_error_cseq_discard=1
bleopt decode_error_kseq_abell=
bleopt decode_error_kseq_vbell=1
bleopt decode_error_kseq_discard=1
bleopt decode_macro_limit=1024
bleopt term_bracketed_paste_mode=on

# Keybindings
# -----------

function go-home {
  cd "$HOME"
}

function go-back {
  cd "$OLDPWD"
}

function go-up {
  cd ..
}

function terminal {
  [[ $TERM != "linux" ]] &&
  [[ -n "${TERMINAL}" ]] &&
  nohup ${TERMINAL} >/dev/null 2>&1 & disown
}

function blerc/sabbrev {
  ble-sabbrev L='| less -R'
}

function blerc/vim-load-hook {
  bleopt prompt_vi_mode_indicator='\q{keymap:vi/mode-indicator}'
  bleopt keymap_vi_mode_show=
  bleopt keymap_vi_mode_name_insert=INSERT
  bleopt keymap_vi_mode_name_replace=REPLACE
  bleopt keymap_vi_mode_name_vreplace=VREPLACE
  bleopt keymap_vi_mode_name_visual=VISUAL
  bleopt keymap_vi_mode_name_select=SELECT
  bleopt keymap_vi_mode_name_linewise=LINE
  bleopt keymap_vi_mode_name_blockwise=BLOCK
  # bleopt keymap_vi_mode_string_nmap:=$'\e[1m-- NORMAL --\e[m'
  bleopt keymap_vi_mode_update_prompt=

  # M-RET -> insert newline
  # ble-decode/keymap:vi_imap/define-meta-bindings

  # ble-bind -m vi_imap -f 'SP' 'self-insert'

  ble-bind -m vi_imap -f 'C-RET' 'accept-line'
  # ble-bind -m vi_imap -f 'C-SP' 'sabbrev-expand'

  ble-bind -m vi_imap -c 'C-j' 'go-back'
  ble-bind -m vi_nmap -c 'C-j' 'go-back'
  ble-bind -m vi_imap -c 'C-k' 'go-up'
  ble-bind -m vi_nmap -c 'C-k' 'go-up'
  ble-bind -m vi_imap -c 'C-h' 'go-home'
  ble-bind -m vi_nmap -c 'C-h' 'go-home'
  ble-bind -m vi_imap -c 'C-o' 'terminal'
  ble-bind -m vi_nmap -c 'C-o' 'terminal'

  ble-bind -m vi_nmap --cursor 2
  ble-bind -m vi_imap --cursor 6
  ble-bind -m vi_omap --cursor 4
  ble-bind -m vi_xmap --cursor 2
  ble-bind -m vi_smap --cursor 2
  ble-bind -m vi_cmap --cursor 0

  # If you don't have the entry Ss in terminfo, yet your terminal supports
  # DECSCUSR, please use the following line to enable DECSCUSR.
  # _ble_term_Ss=$'\e[@1 q'

  # Control sequences that will be output on entering each mode.
  # bleopt term_vi_nmap=
  # bleopt term_vi_imap=
  # bleopt term_vi_omap=
  # bleopt term_vi_xmap=
  # bleopt term_vi_smap=
  # bleopt term_vi_cmap=

  bleopt keymap_vi_imap_undo=
  bleopt keymap_vi_keymodel=
  bleopt keymap_vi_macro_depth=64
  # bleopt keymap_vi_operatorfunc=
  bleopt keymap_vi_search_match_current=

  ble-import vim-surround
  bleopt vim_surround_omap_bind=1
}

blehook/eval-after-load complete blerc/sabbrev
blehook/eval-after-load keymap_vi blerc/vim-load-hook

# Rendering Settings
# ==================

bleopt tab_width=
bleopt char_width_mode=west
bleopt char_width_version=auto
bleopt emoji_width=2
bleopt emoji_version=13.1
bleopt emoji_opts=ri
bleopt grapheme_cluster=extended
bleopt canvas_winch_action=redraw-here

# Colors
# ------

bleopt term_index_colors=256
bleopt term_true_colors=semicolon
bleopt filename_ls_colors="$LS_COLORS"
bleopt highlight_syntax=1
bleopt highlight_filename=1
bleopt highlight_variable=1
bleopt highlight_timeout_sync=500
bleopt highlight_timeout_async=2500
bleopt syntax_eval_polling_interval=100

ble-face -s region                    fg=white,bg=60
ble-face -s region_insert             fg=blue,bg=252
ble-face -s region_match              fg=white,bg=55
ble-face -s region_target             fg=black,bg=153
ble-face -s disabled                  fg=242
ble-face -s overwrite_mode            fg=black,bg=51
ble-face -s auto_complete             fg=238,bg=254
# ble-face -s menu_filter_fixed         bold
# ble-face -s menu_filter_input         fg=16,bg=229
ble-face -s vbell                     reverse
ble-face -s vbell_erase               bg=252
ble-face -s vbell_flash               fg=green,reverse
ble-face -s prompt_status_line        fg=231,bg=240

ble-face -s syntax_default            none
ble-face -s syntax_command            fg=brown
ble-face -s syntax_quoted             fg=green
ble-face -s syntax_quotation          fg=green,bold
ble-face -s syntax_escape             fg=magenta
ble-face -s syntax_expr               fg=navy
ble-face -s syntax_error              bg=203,fg=231
ble-face -s syntax_varname            fg=202
ble-face -s syntax_delimiter          bold
ble-face -s syntax_param_expansion    fg=purple
ble-face -s syntax_history_expansion  bg=94,fg=231
ble-face -s syntax_function_name      fg=92,bold
ble-face -s syntax_comment            fg=gray
ble-face -s syntax_glob               fg=198,bold
ble-face -s syntax_brace              fg=37,bold
ble-face -s syntax_tilde              fg=navy,bold
ble-face -s syntax_document           fg=94
ble-face -s syntax_document_begin     fg=94,bold
ble-face -s command_builtin_dot       fg=red,bold
ble-face -s command_builtin           fg=red
ble-face -s command_alias             fg=teal
ble-face -s command_function          fg=92 # fg=purple
ble-face -s command_file              fg=green
ble-face -s command_keyword           fg=blue
ble-face -s command_jobs              fg=red,bold
ble-face -s command_directory         fg=navy
ble-face -s argument_option           fg=teal
ble-face -s argument_option           fg=black,bg=225
ble-face -s filename_directory        fg=26
ble-face -s filename_directory_sticky fg=white,bg=26
ble-face -s filename_link             fg=teal
ble-face -s filename_orphan           fg=teal,bg=224
ble-face -s filename_setuid           fg=black,bg=220
ble-face -s filename_setgid           fg=black,bg=191
ble-face -s filename_executable       fg=green
ble-face -s filename_other            none
ble-face -s filename_socket           fg=cyan,bg=black
ble-face -s filename_pipe             fg=lime,bg=black
ble-face -s filename_character        fg=white,bg=black
ble-face -s filename_block            fg=yellow,bg=black
ble-face -s filename_warning          fg=red
ble-face -s filename_url              fg=blue
ble-face -s filename_ls_colors        none
ble-face -s varname_array             fg=orange,bold
ble-face -s varname_empty             fg=31
ble-face -s varname_export            fg=200,bold
ble-face -s varname_expr              fg=92,bold
ble-face -s varname_hash              fg=70,bold
ble-face -s varname_number            fg=64
ble-face -s varname_readonly          fg=200
ble-face -s varname_transform         fg=29,bold
ble-face -s varname_unset             fg=124

ble-face -s cmdinfo_cd_cdpath         fg=26,bg=155
