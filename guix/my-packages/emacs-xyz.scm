(define-module (my-packages emacs-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-anti-zenburn-theme
  (package
    (name "emacs-anti-zenburn-theme")
    (version "2.5.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/m00natic/anti-zenburn-theme")
             (commit "dbafbaa86be67c1d409873f57a5c0bbe1e7ca158")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1h4lachmrpjiblah4rjd2cpvz6n6qh3i5cdp4wra2dk177h7kj6h"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/m00natic/anti-zenburn-theme")
    (synopsis "Low contrast grayish Emacs theme, Zenburn palette complemented")
    (description
     "This is a low contrast grayish colour theme derived from Zenburn but with
inverted colours.")
    (license license:gpl3+)))

(define-public emacs-colorless-themes
  (package
    (name "emacs-colorless-themes")
    (version "0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~lthms/colorless-themes.el")
             (commit "c1ed1e12541cf05cc6c558d23c089c07e10b54d7")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "02ai9yf7h3i81bg01w8nb4kdrcc94ladbrkw9vg3p40w617mjwlb"))))
    (build-system emacs-build-system)
    (home-page "https://git.sr.ht/~lthms/colorless-themes.el")
    (synopsis "A collection of mostly colorless themes for Emacs")
    (description
     "Colorless themes is a collection of \"mostly colorless themes\" for
Emacs.")
    (license license:gpl3+)))

(define-public emacs-defrepeater
  (package
    (name "emacs-defrepeater")
    (version "1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/alphapapa/defrepeater.el")
             (commit "9c027a2561fe141dcfb79f75fcaee36cd0386ec1")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1fync4i8ds718pdr2v25ily71jng0506hw5k9qpa0a533nsh7p30"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-s))
    (home-page "https://github.com/alphapapa/defrepeater.el")
    (synopsis "Easily define repeatable Emacs commands")
    (description
      "This package lets you easily define \"repeating commands\", which are
commands that can be repeated by pressing the last key of the sequence bound to
it.")
    (license license:gpl3+)))

(define-public emacs-gotham-theme
  (package
    (name "emacs-gotham-theme")
    (version "1.1.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://depp.brause.cc/gotham-theme.git")
             (commit "4b8214df0851bb69b44c3e864568b7e0030a95d2")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "19ylb7d5jxr7mwjrmgq7acvb4a1q39909xaz3y6s3kjvsyg426pg"))))
    (build-system emacs-build-system)
    (home-page "https://depp.brause.cc/gotham-theme/")
    (synopsis "Code never sleeps in Gotham City")
    (description
     "Gotham is a very dark Emacs color theme. It's a port of the Gotham theme
for Vim and tries adhering closely to the original. There is support for both
GUI and terminal Emacs.")
    (license license:gpl3+)))

(define-public emacs-kaolin-themes
  (package
    (name "emacs-kaolin-themes")
    (version "1.7.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ogdenwebb/emacs-kaolin-themes")
             (commit (string-append "v" version))))
       (sha256
        (base32 "15246nsiwdfy5zl5iml4qxslz8p7k9lrzdr7p6bn71afk721vz5y"))))
    (build-system emacs-build-system)
    (arguments
     (list #:include #~(cons "^themes\\/.*\\.el$" %default-include)))
    (propagated-inputs (list emacs-autothemer))
    (home-page "https://github.com/ogdenwebb/emacs-kaolin-themes")
    (synopsis "Set of eye pleasing themes for GNU Emacs")
    (description
     "Kaolin is a set of eye pleasing themes for GNU Emacs with support for a
large number of modes and external packages. Supports both GUI and terminal.")
    (license license:gpl3+)))

(define-public emacs-nano-theme
  (package
    (name "emacs-nano-theme")
    (version "0.3.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rougier/nano-theme")
             (commit "9d64dc167883835a6dc3a6d286c98dbbf7e95a96")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0r8v9khdv9sjl7nl4m0cf4wrrnkqgnambqj3h491ykkpqd1h2s4y"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/rougier/nano-theme")
    (synopsis "A consistent theme for GNU Emacs")
    (description
     "N Λ N O theme is a consistent theme that is fully defined by a set of
(1+6) faces as explained in \"On the Design of Text Editors\" /
https://arxiv.org/abs/2008.06030")
    (license license:gpl3+)))

(define-public emacs-notink-theme
  (package
    (name "emacs-notink-theme")
    (version "1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/MetroWind/notink-theme")
             (commit "6115857fe75c1adbbce4165a2b77a11a271aaf31")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07gr1apbhd4kli2g0ld4yzpsc9hvkrh054b2dk47l2p9d1ki1j7g"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/MetroWind/notink-theme")
    (synopsis "A grey-scale Emacs theme inspired by e-ink displays")
    (description
     "A custom Emacs theme inspired by e-ink displays, for people who are tired
of colorful colors, and die-hard minimalinimilists.")
    (license license:wtfpl2)))

(define-public emacs-plan9-theme
  (package
    (name "emacs-plan9-theme")
    (version "0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/john2x/plan9-theme.el")
             (commit "c2da2fcb241e9800d931a1ff19ecd9fd84d30382")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0rjxbk9fljnjmg00vdqcyynzg591cgknyy2d92xsxsyg4d28dvwi"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/john2x/plan9-theme.el")
    (synopsis "Emacs theme inspired by the Plan9 project")
    (description
     "Emacs theme inspired by Plan 9 colors. A light alternative for that
classic look.")
    (license license:gpl3+)))

(define-public emacs-tron-legacy-theme
  (package
    (name "emacs-tron-legacy-theme")
    (version "2.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ianyepan/tron-legacy-emacs-theme")
             (commit "c707baf08516f69a88ca2f494a9556ac6d1986f4")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ibvx8zqym0g9jalmill5514gr24b4yslns4lzcqxr1s7ibs671r"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/ianyepan/tron-legacy-emacs-theme")
    (synopsis "Original retro-futuristic Emacs theme inspired by Tron: Legacy")
    (description
     "Original retro-futuristic Emacs theme inspired by Tron: Legacy,
Base16-Black-Metal, Grayscale and City Lights.")
    (license license:gpl3+)))
