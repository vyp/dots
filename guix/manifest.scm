(use-modules (gnu packages base)
             (gnu packages certs)
             (gnu packages curl)
             (gnu packages emacs)
             (gnu packages emacs-xyz)
             (gnu packages fonts)
             (gnu packages fontutils)
             (gnu packages ghostscript)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages imagemagick)
             (gnu packages maths)
             (gnu packages pdf)
             (gnu packages terminals)
             (gnu packages tex)
             (gnu packages texlive)
             (gnu packages version-control)
             (gnu packages video)
             (gnu packages wget)
             (my-packages emacs-xyz))

(define essential-packages
  (list glibc-locales
        font-dejavu
        font-ghostscript
        font-gnu-freefont
        fontconfig
        nss-certs))

;; Emacs packages not packaged:
;;
;; - emacs-defrepeater
;;
;; Emacs themes packaged:
;;
;; - emacs-base16-theme
;; - emacs-moe-theme
;; - emacs-punpun-theme
;; - emacs-tao-theme
;; - emacs-zenburn-theme

(define emacs-packages
  (list emacs-corfu
        emacs-corfu-terminal
        emacs-dash
        emacs-defrepeater
        emacs-evil
        emacs-evil-collection
        emacs-evil-commentary
        emacs-evil-surround
        emacs-general
        emacs-rainbow-delimiters
        emacs-s
        emacs-undo-fu
        emacs-use-package
        emacs-which-key))

(define emacs-themes
  (list emacs-anti-zenburn-theme
        emacs-base16-theme
        ;; emacs-colorless-themes
        emacs-ef-themes
        emacs-gotham-theme
        emacs-kaolin-themes
        emacs-moe-theme-el
        ;; emacs-nano-theme
        emacs-notink-theme
        emacs-plan9-theme
        emacs-solarized-theme
        emacs-tron-legacy-theme
        emacs-zenburn-theme))

(define font-packages
  (list font-iosevka
        font-libertinus
        texlive-ebgaramond
        texlive-garamond-math))

(packages->manifest
 (append
  essential-packages
  font-packages
  (list emacs)
  emacs-packages
  emacs-themes
  (list curl
        imagemagick
        git
        gnuplot
        guile-3.0
        guile-f-scm
        guile-pipe
        mpv
        stapler
        texlive
        wget
        youtube-dl
        yt-dlp
        zathura
        zathura-pdf-mupdf)))
