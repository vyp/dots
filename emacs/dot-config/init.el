;;; -*- lexical-binding: t; -*-

;; * Generic Minor Modes

;; ** Essentials
(setq use-package-always-defer t)

(use-package dash :demand t)
(use-package s :demand t)

(use-package general
  :demand t

  :config
  (general-auto-unbind-keys :off)
  (general-override-mode)

  (eval-and-compile
    (defalias 'gsetq #'general-setq)
    (defalias 'gsetql #'general-setq-local)
    (defalias 'gsetqd #'general-setq-default)
    (defalias 'gadd-hook #'general-add-hook)
    (defalias 'gadd-advice #'general-add-advice)
    (defalias 'grm-hook #'general-remove-hook)
    (defalias 'grm-advice #'general-remove-advice)
    (defalias 'my/fg #'set-face-foreground)
    (defalias 'my/bg #'set-face-background)
    (defalias 'my/bold #'set-face-bold)
    (defalias 'my/ital #'set-face-italic))

  (defmacro my/attr (face &rest attrs)
    (let* ((f (lambda (value)
                (cond ((eq value :fg) :foreground)
                      ((eq value :bg) :background)
                      (t value))))
           (attrs (mapcar f attrs)))
      `(set-face-attribute ,face nil ,@attrs)))

  (defmacro my/defer-with-selected-frame-if-daemonp (f)
    (let ((frame (gensym "frame")))
      `(if (daemonp)
           (gadd-hook 'after-make-frame-functions
                      (lambda (,frame)
                        (with-selected-frame ,frame
                          (funcall ,f ,frame))))
         (funcall ,f nil))))

  (defun my/render-env (frame)
    (cond ((display-graphic-p frame) 'gui)
          ((getenv "DISPLAY" frame) 'term)
          (t 'tty)))

  (defun my/true-color-term-p (frame)
    (and (eq (my/render-env frame) 'term)
         (= (tty-display-color-cells) 16777216)))

  (defun my/after-focus-change (gui-proc tui-proc)
    (let* ((states
            (->>
             (frame-list)
             (--map (list (my/render-env it) (frame-focus-state it)))))
           (focused-guis
            (->>
             states
             ;; Focused guis.
             (--count (and (eq (-first-item it) 'gui)
                           (eq (-second-item it) t)))))
           (focused-or-unknown-nonguis
            (->>
             states
             (--count (and (not (eq (-first-item it) 'gui))
                           (not (null (-second-item it))))))))
      (if (and (> focused-guis 0)
               (= focused-or-unknown-nonguis 0))
          (funcall gui-proc)
        (funcall tui-proc))))

  ;; Globals.
  (gsetq my/render-env (unless (daemonp) (my/render-env nil))
         my/user-rc-dir (expand-file-name "~/dots")))

(use-package defrepeater :demand t)

(use-package undo-fu
  :init
  (gsetq undo-fu-ignore-keyboard-quit t
         ;; 64 MB.
         undo-limit 6710886400
         ;; 96 MB.
         undo-strong-limit 100663296
         ;; 960 MB.
         undo-outer-limit 1006632960))

(use-package evil
  :init
  (gsetq evil-cross-lines t
         evil-overriding-maps nil
         evil-shift-width 2
         evil-split-window-below t
         evil-undo-system 'undo-fu
         evil-vsplit-window-right t
         evil-want-C-d-scroll t
         evil-want-C-u-scroll t
         evil-want-integration t
         evil-want-keybinding nil
         evil-want-Y-yank-to-eol t
         ;; Make gj/gk work across visually wrapped lines.
         line-move-visual nil)

  (evil-mode 1)

  :config
  (defun my/term-cur (shape)
    (when (or (eq my/render-env 'term)
              ;; Or if in daemon.
              (and (not my/render-env)
                   (eq (my/render-env (selected-frame)) 'term)))
      (->>
       (cond ((eq shape 'box) "2")
             ((eq shape 'hbar) "4")
             ((eq shape 'bar) "6"))
       (funcall (-cut concat "\e[" <> " q"))
       (funcall (-cut send-string-to-terminal <>)))))

  (gadd-hook 'evil-normal-state-entry-hook (-cut my/term-cur 'box))
  (gadd-hook 'evil-insert-state-entry-hook (-cut my/term-cur 'bar))
  (gadd-hook 'evil-visual-state-entry-hook (-cut my/term-cur 'box))
  (gadd-hook 'evil-replace-state-entry-hook (-cut my/term-cur 'box))
  (gadd-hook 'evil-operator-pending-state-entry-hook (-cut my/term-cur 'hbar))
  (gadd-hook 'evil-motion-state-entry-hook (-cut my/term-cur 'box))
  (gadd-hook 'evil-emacs-state-entry-hook (-cut my/term-cur 'bar))

  ;; Move up one line when doing q: and q/.
  (gadd-advice 'evil-command-window-ex :after #'evil-previous-line)
  (gadd-advice 'evil-command-window-search-forward :after #'evil-previous-line)

  ;; Set normal state as default state. (Under :config as it references evil
  ;; mode variables.)
  (->> (append evil-emacs-state-modes evil-normal-state-modes)
       (gsetq evil-normal-state-modes))

  (gsetq evil-emacs-state-modes nil
         evil-motion-state-modes nil)

  ;; Make the window resizing commands repeatable.
  ;;
  ;; The disadvantage of defrepeater compared to a [hydra] is that it only works
  ;; one command at a time, i.e. wanting to use another command with the same
  ;; prefix sequence requires starting the sequence again.
  ;;
  ;; But the advantage here is the rest of evil-window-map commands exit
  ;; automatically, which is better for one off commands.
  ;;
  ;; [hydra]: https://github.com/abo-abo/hydra
  (defrepeater #'evil-window-increase-height)
  (defrepeater #'evil-window-decrease-height)
  (defrepeater #'evil-window-increase-width)
  (defrepeater #'evil-window-decrease-width)

  (general-def
    [remap evil-window-increase-height] #'evil-window-increase-height-repeat
    [remap evil-window-decrease-height] #'evil-window-decrease-height-repeat
    [remap evil-window-increase-width] #'evil-window-increase-width-repeat
    [remap evil-window-decrease-width] #'evil-window-decrease-width-repeat)

  (general-create-definer general-my/leader
    :states '(normal visual)
    :keymaps 'override
    :prefix "m")

  ;; TODO: `shell-command-on-region` to copy selection to clipboard when in
  ;; terminal emulator.
  (general-def '(normal visual) 'override
    "\\" #'evil-switch-to-windows-last-buffer
    "'" #'evil-ex
    "+" #'text-scale-increase
    "-" #'text-scale-decrease
    "H" #'evil-first-non-blank
    "L" #'evil-end-of-line
    "M" #'evil-jump-item
    "C-n" #'next-buffer
    "C-p" #'previous-buffer
    "C-h" #'evil-window-left
    "C-j" #'evil-window-down
    "C-k" #'evil-window-up
    "C-l" #'evil-window-right
    "C-/" #'help-command
    ;; C-_ seems to map to C-/ in the non-gui environments.
    "C-_" #'help-command)

  (general-def evil-window-map
    "d" #'evil-window-delete)

  (general-def 'insert 'override
    "C-h" #'backward-char
    "C-l" #'forward-char
    "C-/" #'help-command
    "C-_" #'help-command)

  (general-def 'normal 'override
    "Q" #'kill-this-buffer)

  (general-def 'normal
    "gs" #'evil-write)

  (general-my/leader
    ";" #'evil-command-window-ex
    "?" #'help-command
    "d" #'kill-buffer
    "e" #'eval-expression
    "l" #'count-lines-page
    "t" #'load-theme
    "u" #'universal-argument
    "w" #'evil-window-map
    "x" #'execute-extended-command)

  ;; Escape everywhere.
  (general-def 'emacs "<escape>" #'evil-normal-state)
  (general-def
    '(minibuffer-local-map
      minibuffer-local-ns-map
      minibuffer-local-completion-map
      minibuffer-local-must-match-map
      minibuffer-local-isearch-map)
    "<escape>" #'keyboard-escape-quit))

(use-package evil-surround
  :init
  (global-evil-surround-mode))

(use-package evil-commentary
  :demand t
  :config
  ;; Not using minor mode since it sets a super keybinding which I do not like,
  ;; but it means these become global keybindings.
  (general-def 'normal
    "gc" #'evil-commentary))

(use-package which-key
  :init
  (which-key-mode))

(use-package evil-collection
  :init
  (gsetq evil-collection-mode-list nil)
  (evil-collection-init))

;; ** Auxiliary Files
(gsetq auto-save-list-file-prefix nil
       create-lockfiles nil
       ;; NOTE: We never load this file since it should always be empty.
       custom-file (expand-file-name "emacs/custom.el" my/user-rc-dir)
       make-backup-files nil)

;; ** Display

;; TODO: hl-line mode.
;; TODO: Beacon mode for when point wraps around buffer?

(gsetqd auto-fill-function 'do-auto-fill
        fill-column 80
        ;; Do not insert tabs when indenting.
        indent-tabs-mode nil
        ;; Vertical cursor padding.
        scroll-margin 5
        scroll-step 1
        tab-width 8)

;; Relevant for filling.
(gsetq sentence-end-double-space nil)

(use-package avoid
  :init
  (->>
   '((frame-or-window . frame)
     (side . right)
     (side-pos . 0)
     (top-or-bottom . bottom)
     (top-or-bottom-pos . 0))
   (gsetq mouse-avoidance-banish-position))

  (mouse-avoidance-mode 'banish))

(use-package files
  :init
  (gsetq enable-local-variables :safe
         enable-local-eval nil
         require-final-newline t))

(use-package frame
  :init
  (gsetq visible-cursor nil)
  (blink-cursor-mode -1))

(use-package paren
  :init
  (gsetq show-paren-delay 0)
  ;; (gsetq show-paren-highlight-openparen nil)
  (show-paren-mode 1))

(use-package simple
  :ghook ('(text-mode-hook prog-mode-hook) #'visual-line-mode)

  :init
  ;; Show fringe indicators for visually wrapped lines.
  (gsetq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

  (column-number-mode 1))

(use-package subword
  :init
  (global-subword-mode 1))

(use-package tooltip
  :init
  (tooltip-mode -1))

(use-package whitespace
  :ghook 'text-mode-hook 'prog-mode-hook

  :init
  (gsetq whitespace-line-column fill-column
         whitespace-style '(face empty lines-tail tabs trailing)))

;; ** Auto-Completion
(use-package corfu
  :init
  (gsetq corfu-cycle t
         corfu-preselect-first nil
         tab-always-indent 'complete)

  (general-def 'insert 'corfu-map
    "<escape>" #'evil-collection-corfu-quit-and-escape
    "TAB" #'corfu-next
    [tab] #'corfu-next
    [S-tab] #'corfu-previous
    [backtab] #'corfu-previous)

  (evil-collection-init 'corfu)
  (global-corfu-mode 1))

(use-package corfu-terminal
  :init
  (my/defer-with-selected-frame-if-daemonp
   (lambda (frame)
     (unless (eq (my/render-env frame) 'gui)
       (corfu-terminal-mode 1)))))

;; ** Navigation
(use-package vc-hooks
  :init
  (gsetq vc-follow-symlinks t))

;; ** Linting
(use-package warnings
  :init
  (gsetq warning-suppress-types '((comp))))

;; * Applications

;; ** Splash Screen
(defun my/first-file-from-cli-args ()
  (let ((files-from-cli-args (seq-filter #'buffer-file-name (buffer-list))))
    (if (> (safe-length files-from-cli-args) 0)
        (car files-from-cli-args)
      (get-buffer "*scratch*"))))

(gsetq initial-buffer-choice #'my/first-file-from-cli-args)

;; Got this ascii art from [[https://github.com/ryukinix/dotfiles]].
(gsetq initial-scratch-message "\
;; ▓█████  ███▄ ▄███▓ ▄▄▄       ▄████▄    ██████
;; ▓█   ▀ ▓██▒▀█▀ ██▒▒████▄    ▒██▀ ▀█  ▒██    ▒
;; ▒███   ▓██    ▓██░▒██  ▀█▄  ▒▓█    ▄ ░ ▓██▄
;; ▒▓█  ▄ ▒██    ▒██ ░██▄▄▄▄██ ▒▓▓▄ ▄██▒  ▒   ██▒
;; ░▒████▒▒██▒   ░██▒ ▓█   ▓██▒▒ ▓███▀ ░▒██████▒▒
;; ░░ ▒░ ░░ ▒░   ░  ░ ▒▒   ▓▒█░░ ░▒ ▒  ░▒ ▒▓▒ ▒ ░
;; ░ ░  ░░  ░      ░  ▒   ▒▒ ░  ░  ▒   ░ ░▒  ░ ░
;; ░   ░      ░     ░   ▒   ░        ░  ░  ░
;; ░  ░       ░         ░  ░░ ░            ░

")

;; Single window on startup.
(gadd-hook 'emacs-startup-hook #'delete-other-windows)

;; ** Shell

;; ** Buffer Manager

;; ** File Manager

;; ** Git

;; ** IRC

;; ** Mail

;; * Text Editing

;; ** Document

;; ** Data

;; ** Functional

;; *** Lisp
(use-package rainbow-delimiters
  :ghook 'prog-mode-hook

  :init
  (defconst my/rd-faces
    (->>
     (number-sequence 1 9)
     (mapcar #'number-to-string)
     (--map (concat "rainbow-delimiters-depth-" it "-face"))
     (mapcar #'intern)))

  (defmacro my/rd (f &rest attrs)
    (->>
     my/rd-faces
     (--map `(funcall ,f ',it ,@attrs))
     (cons 'progn)))

  ;; NOTE: Does not work with `my/attr` as f.
  (defmacro my/toggle-rd (f v1 v2)
    (->>
     (number-sequence 1 9)
     (--map (if (= (mod it 2) 1) v1 v2))
     (-zip-lists my/rd-faces)
     (--map `(',(car it) ,@(cdr it)))
     (--map `(funcall ,f ,@it))
     (cons 'progn)))

  :config
  (my/custom-theme-overrides (car custom-enabled-themes)))

(use-package scheme
  :config
  (put 'eval-when 'scheme-indent-function 1)
  (put 'call-with-prompt 'scheme-indent-function 1)
  (put 'test-assert 'scheme-indent-function 1)
  (put 'test-assertm 'scheme-indent-function 1)
  (put 'test-equalm 'scheme-indent-function 1)
  (put 'test-equal 'scheme-indent-function 1)
  (put 'test-eq 'scheme-indent-function 1)
  (put 'call-with-input-string 'scheme-indent-function 1)
  (put 'call-with-port 'scheme-indent-function 1)
  (put 'guard 'scheme-indent-function 1)
  (put 'lambda* 'scheme-indent-function 1)
  (put 'substitute* 'scheme-indent-function 1)
  (put 'match-record 'scheme-indent-function 2)

  ;; 'modify-inputs' and its keywords.
  (put 'modify-inputs 'scheme-indent-function 1)
  (put 'replace 'scheme-indent-function 1)

  ;; 'modify-phases' and its keywords.
  (put 'modify-phases 'scheme-indent-function 1)
  (put 'replace 'scheme-indent-function 1)
  (put 'add-before 'scheme-indent-function 2)
  (put 'add-after 'scheme-indent-function 2)

  (put 'modify-services 'scheme-indent-function 1)
  (put 'with-directory-excursion 'scheme-indent-function 1)
  (put 'with-file-lock 'scheme-indent-function 1)
  (put 'with-file-lock/no-wait 'scheme-indent-function 1)
  (put 'with-profile-lock 'scheme-indent-function 1)
  (put 'with-writable-file 'scheme-indent-function 2)

  (put 'package 'scheme-indent-function 0)
  (put 'package/inherit 'scheme-indent-function 1)
  (put 'origin 'scheme-indent-function 0)
  (put 'build-system 'scheme-indent-function 0)
  (put 'bag 'scheme-indent-function 0)
  (put 'gexp->derivation 'scheme-indent-function 1)
  (put 'graft 'scheme-indent-function 0)
  (put 'operating-system 'scheme-indent-function 0)
  (put 'file-system 'scheme-indent-function 0)
  (put 'manifest-entry 'scheme-indent-function 0)
  (put 'manifest-pattern 'scheme-indent-function 0)
  (put 'substitute-keyword-arguments 'scheme-indent-function 1)
  (put 'with-store 'scheme-indent-function 1)
  (put 'with-external-store 'scheme-indent-function 1)
  (put 'with-error-handling 'scheme-indent-function 0)
  (put 'with-mutex 'scheme-indent-function 1)
  (put 'with-atomic-file-output 'scheme-indent-function 1)
  (put 'call-with-compressed-output-port 'scheme-indent-function 2)
  (put 'call-with-decompressed-port 'scheme-indent-function 2)
  (put 'call-with-gzip-input-port 'scheme-indent-function 1)
  (put 'call-with-gzip-output-port 'scheme-indent-function 1)
  (put 'call-with-lzip-input-port 'scheme-indent-function 1)
  (put 'call-with-lzip-output-port 'scheme-indent-function 1)
  (put 'signature-case 'scheme-indent-function 1)
  (put 'emacs-batch-eval 'scheme-indent-function 0)
  (put 'emacs-batch-edit-file 'scheme-indent-function 1)
  (put 'emacs-substitute-sexps 'scheme-indent-function 1)
  (put 'emacs-substitute-variables 'scheme-indent-function 1)
  (put 'with-derivation-narinfo 'scheme-indent-function 1)
  (put 'with-derivation-substitute 'scheme-indent-function 2)
  (put 'with-status-report 'scheme-indent-function 1)
  (put 'with-status-verbosity 'scheme-indent-function 1)
  (put 'with-build-handler 'scheme-indent-function 1)

  (put 'mlambda 'scheme-indent-function 1)
  (put 'mlambdaq 'scheme-indent-function 1)
  (put 'syntax-parameterize 'scheme-indent-function 1)
  (put 'with-monad 'scheme-indent-function 1)
  (put 'mbegin 'scheme-indent-function 1)
  (put 'mwhen 'scheme-indent-function 1)
  (put 'munless 'scheme-indent-function 1)
  (put 'mlet* 'scheme-indent-function 2)
  (put 'mlet 'scheme-indent-function 2)
  (put 'mparameterize 'scheme-indent-function 2)
  (put 'run-with-store 'scheme-indent-function 1)
  (put 'run-with-state 'scheme-indent-function 1)
  (put 'wrap-program 'scheme-indent-function 1)
  (put 'wrap-script 'scheme-indent-function 1)
  (put 'with-imported-modules 'scheme-indent-function 1)
  (put 'with-extensions 'scheme-indent-function 1)
  (put 'with-parameters 'scheme-indent-function 1)
  (put 'let-system 'scheme-indent-function 1)
  (put 'with-build-variables 'scheme-indent-function 2)

  (put 'with-database 'scheme-indent-function 2)
  (put 'call-with-database 'scheme-indent-function 1)
  (put 'call-with-transaction 'scheme-indent-function 1)
  (put 'with-statement 'scheme-indent-function 3)
  (put 'call-with-retrying-transaction 'scheme-indent-function 1)
  (put 'call-with-savepoint 'scheme-indent-function 1)
  (put 'call-with-retrying-savepoint 'scheme-indent-function 1)

  (put 'call-with-container 'scheme-indent-function 1)
  (put 'container-excursion 'scheme-indent-function 1)
  (put 'eventually 'scheme-indent-function 1)

  (put 'call-with-progress-reporter 'scheme-indent-function 1)
  (put 'with-repository 'scheme-indent-function 2)
  (put 'with-temporary-git-repository 'scheme-indent-function 2)
  (put 'with-environment-variables 'scheme-indent-function 1)
  (put 'with-fresh-gnupg-setup 'scheme-indent-function 1)

  (put 'with-paginated-output-port 'scheme-indent-function 1)

  (put 'with-shepherd-action 'scheme-indent-function 3)

  (put 'with-http-server 'scheme-indent-function 1)

  ;; Emacs 28 changed the behavior of 'lisp-fill-paragraph', which causes the
  ;; first line of package descriptions to extrude past 'fill-column', and
  ;; somehow that is deemed more correct upstream (see: [guix-issue-56197]).
  ;;
  ;; [guix-issue-56197]: https://issues.guix.gnu.org/56197
  (require 'lisp-mode)

  (defun emacs27-lisp-fill-paragraph (&optional justify)
    (interactive "P")
    (or (fill-comment-paragraph justify)
        (let ((paragraph-start
               (concat paragraph-start
                       "\\|\\s-*\\([(;\"]\\|\\s-:\\|`(\\|#'(\\)"))
              (paragraph-separate
               (concat paragraph-separate "\\|\\s-*\".*[,\\.]$"))
              (fill-column
               (if (and (integerp emacs-lisp-docstring-fill-column)
                        (derived-mode-p 'emacs-lisp-mode))
                   emacs-lisp-docstring-fill-column
                 fill-column)))
          (fill-paragraph justify))
        ;; Never return nil.
        t))

  (gsetql fill-paragraph-function #'emacs27-lisp-fill-paragraph))

;; ** Other High Level

;; *** Dynamic

;; *** Static

;; ** Systems

;; * Mode Line
(defun my/mode-line-padding (right-length)
  (propertize
   " "
   'display
   `((space
      :align-to
      (- (+ right right-fringe right-margin)
         ,right-length)))))

(gsetqd
 mode-line-format
 (list
  ;; Full memory error message.
  " %e"

  ;; Whether frame is an emacsclient instance or not.
  (if (daemonp) " @  " "  ")

  ;; Read only indicator or modified indicator if not read only.
  '(:eval (if buffer-read-only
              "[RO]"
            (if (buffer-modified-p)
                "*"
              "")))

  ;; Separator.
  "  "

  ;; Buffer.
  "%b"
  ; '(:eval (propertize "%b" 'face 'bold))

  ;; Insert spaces to right align rest.
  ;; Works because max length of right aligned text is known beforehand.
  ;; TODO: Switch to using mode-line-format-right-align for Emacs 30?
  ;; TODO: Can also use mode-line-end-spaces to make this less arbitrary? (In
  ;; terms of number of padding spaces.)
  '(:eval (my/mode-line-padding 21))

  ;; Line number.
  "%l,"

  ;; Column number.
  "%C"

  ;; Separator.
  '(:eval (my/mode-line-padding 8))

  ;; Buffer position.
  "%06p  "))

;; * Theme
(defun my/reset-term-palette (&optional frame)
  (when (my/true-color-term-p frame)
    (->>
     (number-sequence 0 7)
     (mapcar #'number-to-string)
     (funcall (-cut mapconcat #'identity <> ";"))
     (funcall (-cut concat "\e]104;" <> "\e\\"))
     (funcall (-cut send-string-to-terminal <> frame)))))

(defun my/reset-terms ()
  (if (daemonp)
      (mapc #'my/reset-term-palette (frame-list))
    (my/reset-term-palette nil)))

;; Emacs manual says colors #000001 - #000007 are actually used as indexed
;; (terminal palette) colors even in true color mode. However, color #000000
;; also seems to be used as indexed color. (Set your terminal palette color 0 to
;; something other than #000000 and M-x list-colors-display should show black as
;; using your terminal color 0 instead of actual black color.) So we use OSC
;; escape sequences 4 and 104 respectively to set and reset our terminal palette
;; for colors 0-8 upon entering and exiting emacs from a terminal when using
;; true color capabilities, but support for OSC escape sequences 4 and 104 is
;; not checked for.
;;
;; https://www.gnu.org/software/emacs/manual/html_node/efaq/Colors-on-a-TTY.html
(my/defer-with-selected-frame-if-daemonp
 (lambda (frame)
   (when (my/true-color-term-p frame)
     (dotimes (i 8)
       (->>
        (number-to-string i)
        (funcall (lambda (it) (concat "\e]4;" it ";#00000" it "\e\\")))
        (send-string-to-terminal)))

     (gadd-hook 'delete-frame-functions #'my/reset-term-palette)

     ;; NOTE: Does not always seem to work for daemon. Therefore best practice
     ;; would be to always close terminal clients before killing daemon, so that
     ;; terminal palettes get reset.
     (gadd-hook 'kill-emacs-hook #'my/reset-terms))))

(use-package base16-theme
  :init
  (gsetq base16-theme-256-color-source 'colors))

(use-package anti-zenburn-theme)
;; ;; NOTE: Not on melpa. <https://github.com/blobject/cemant>
;; (use-package cemant-theme)
;; (use-package colorless-themes)
;; (use-package constant-theme)
(use-package gotham-theme)
;; (use-package greymatters-theme)
;; (use-package habamax-theme)
(use-package kaolin-themes)
;; (use-package kosmos-theme)
;; (use-package metalheart-theme)
(use-package moe-theme)
(use-package notink-theme)
;; (use-package parchment-theme)
(use-package plan9-theme)
;; (use-package punpun-theme)
;; (use-package tao-theme)
(use-package tron-legacy-theme)
(use-package zenburn-theme)

;; NOTE: These face modifications will "leak" when switching themes (unless the
;; theme or the theme modifications here overwrite the modifications), since
;; they are not part of the themes themselves and therefore `disable-theme` will
;; not undo them.
;;
;; TODO: No underline for button face.
(defun my/custom-theme-overrides (theme &rest _)
  (pcase theme
    ('anti-zenburn
     (my/bold 'font-lock-builtin-face nil)
     (my/bold 'font-lock-keyword-face nil)
     (my/bold 'font-lock-function-name-face t)

     (when (featurep 'rainbow-delimiters)
       (my/toggle-rd #'my/fg "#834744" "#232333")))

    ('base16-black-metal-immortal
     (my/fg 'default "#888888")
     (my/fg 'font-lock-builtin-face "#888888")
     (my/fg 'font-lock-constant-face "#888888")
     (my/fg 'font-lock-keyword-face "#888888")
     (my/fg 'font-lock-function-name-face "#aaaaaa")

     (when (featurep 'rainbow-delimiters)
       (my/toggle-rd #'my/fg "#333333" "#556677")))

    ('base16-grayscale-light
     (when (featurep 'rainbow-delimiters)
       (my/toggle-rd #'my/fg "#464646" "#ababab")))

    ('base16-icy
     (my/fg 'font-lock-builtin-face "#095b67")
     (my/fg 'font-lock-constant-face "#095b67")
     (my/fg 'font-lock-function-name-face "#b3ebf2")
     (my/fg 'font-lock-variable-name-face "#80deea")
     (my/fg 'font-lock-type-face "#095b67")
     (my/attr 'show-paren-match :fg "#16c1d9" :bg "#021012")

     (when (featurep 'rainbow-delimiters)
       (my/toggle-rd #'my/fg "#095b67" "#052e34")))

    ((pred (lambda (theme) (s-prefix? "ef-" (symbol-name theme))))
     (my/ital 'font-lock-comment-face nil)
     (my/bold 'font-lock-keyword-face nil))

    ('kaolin-mono-dark
     (my/attr 'isearch :underline nil)

     (when (featurep 'rainbow-delimiters)
       (my/toggle-rd #'my/fg "#40826d" "#41544B")))

    ('notink
     (my/ital 'font-lock-comment-face nil)
     (my/ital 'font-lock-constant-face nil)
     (my/ital 'font-lock-keyword-face nil)
     (my/bold 'font-lock-function-name-face t)
     (my/bold 'font-lock-variable-name-face t)

     (when (featurep 'rainbow-delimiters)
       (my/fg 'rainbow-delimiters-depth-8-face "#83919a")
       (my/fg 'rainbow-delimiters-depth-9-face "#4c5256")))

    ('parchment
     (my/ital 'font-lock-comment-face nil)
     (my/bold 'font-lock-function-name-face t)
     (my/fg 'font-lock-type-face "#000000")
     (my/fg 'font-lock-variable-name-face "#004488"))

    ((or 'punpun-dark 'punpun-light)
     (my/attr 'font-lock-function-name-face :slant 'normal :weight 'bold)
     (my/ital 'font-lock-type-face nil)
     (my/attr 'font-lock-variable-name-face :slant 'normal :weight 'bold)

     (pcase theme
       ('punpun-dark
        (my/fg 'font-lock-comment-face "#3a3a3a")
        (my/attr 'show-paren-match :fg "#080808" :bg "#949494"))

       ('punpun-light
        (my/fg 'font-lock-comment-face "#b2b2b2")
        (my/attr 'show-paren-match :fg "#eeeeee" :bg "#585858"))))

    ('kosmos
     (my/fg 'font-lock-keyword-face "#bdbdbd")
     (my/bold 'font-lock-function-name-face t))

    ('zenburn
     (my/bold 'font-lock-keyword-face nil)))

  ;; Always bold delimiters regardless of theme, as they are easier to see and
  ;; identify for me.
  (when (featurep 'rainbow-delimiters)
    (my/rd #'my/bold t)))

;; https://emacs.stackexchange.com/questions/3112/how-to-reset-color-theme
(defun my/undo-themes (&rest _)
  (mapc #'disable-theme custom-enabled-themes))

(gadd-advice 'load-theme :before #'my/undo-themes)
(gadd-advice 'load-theme :after #'my/custom-theme-overrides)

;; Set a theme on startup.
(my/defer-with-selected-frame-if-daemonp
 (lambda (frame)
   (load-theme 'base16-icy 'no-confirm)))
