;; Prevent Emacs from creating "~/.emacs.d/auto-save-list" directory on startup.
(setq auto-save-default nil)
;; Using Guix.
(setq package-enable-at-startup nil)
(setq-default major-mode 'text-mode)
;; Support more than 8 colors in Foot terminal.
(add-to-list 'term-file-aliases '("foot" . "xterm"))

;; Disable some gui elements.
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; Main monospace font.
; (push '(font . "Iosevka Custom-12") default-frame-alist)
(push '(font . "Iosevka-12") default-frame-alist)
