# vim: ft=sh

# Exit if non-interactive shell.
if [[ $- != *i* ]]; then
  exit 0
fi

# source $(blesh-share)/ble.sh --noattach

# -----------------------------------------------------------------------------
# Disable start/stop signals (therefore allows to use ^s in keybindings).
stty -ixon

# Vi keybindings.
set -o vi

# Make ESC faster.
stty time 0
builtin bind 'set keyseq-timeout 1'

builtin bind 'set bell-style none'
shopt -s autocd extglob

# Disables annoying GTK authentication popup for example when doing `git push'.
unset SSH_ASKPASS

# -----------------------------------------------------------------------------
HISTCONTROL='ignoreboth:erasedups'
# Lines of history to keep in memory.
HISTSIZE=500
# Maximum number of lines for history file.
HISTFILESIZE=200000

# -----------------------------------------------------------------------------
# function build {
#   nix-build -E 'with import <nixpkgs> { }; callPackage ./default.nix { }'
# }

# function hack {
#   nix-shell -E 'with import <nixpkgs> { }; callPackage ./default.nix { }'
# }

# Send 'urgent' bell to terminal.
function bell {
  echo -e "\a"
}

# Enter directory.
function ed {
  mkdir -p "$1"
  cd ./"$1"
}

function go-back {
  cd "$OLDPWD"
  tput setaf 3
  printf ' '
  pwd
}

function go-home {
  cd
  tput setaf 3
  printf ' '
  pwd
}

function go-up {
  cd ..
  tput setaf 3
  printf ' '
  pwd
}

function terminal {
  [[ $TERM != "linux" ]] &&
  [[ -n "${TERMINAL}" ]] &&
  nohup ${TERMINAL} >/dev/null 2>&1 & disown
}

builtin bind -m vi -x '"\C-h":go-home'
builtin bind -m vi-insert -x '"\C-h":go-home'
builtin bind -m vi -x '"\C-j":go-back'
builtin bind -m vi-insert -x '"\C-j":go-back'
builtin bind -m vi -x '"\C-k":go-up'
builtin bind -m vi-insert -x '"\C-k":go-up'
builtin bind -m vi -x '"\C-l":clear'
builtin bind -m vi-insert -x '"\C-l":clear'
builtin bind -m vi -x '"\C-o":terminal'
builtin bind -m vi-insert -x '"\C-o":terminal'

# -----------------------------------------------------------------------------
alias ab='acpi -b'
alias am='alsamixer'
alias bl='bell'
alias c='cat'
alias cp='cp -v'
alias cpr='cp -vr'
alias e='emacs -nw'
alias em='emacs'
alias ecli='emacsclient -a "" -nw'
alias egui='emacsclient -a "" -c'
alias ekill='emacsclient -e "(kill-emacs)"'
alias f='file'
alias ga='git add'
alias gall='git add -A'
alias gap='git add -p'
alias gch='git checkout'
alias gcm='git commit'
alias gcmm='git commit --edit -m \
  "$(git log --format=%B --reverse HEAD..HEAD@{1})"'
alias gd='git diff'
alias gdc='git diff --check'
alias gds='git diff --staged'
alias gdsc='git diff --staged --check'
alias gg='git grep'
alias ggi='git grep -i'
alias gl='git log'
alias gld='git log -p'
alias gpi='grep -i'
alias gpsh='git push'
alias grs='git reset'
alias gs='git status'
alias gsh='git show'
alias gshd='git show HEAD'
alias info='info --vi-keys'
alias l='less -R'
alias ls='ls -p --color'
alias l1='ls -p1 --color'
alias la='ls -A --color'
alias la1='ls -A1 --color'
alias lah='ls -lAh --color'
alias lh='ls -lh --color'
alias m='mpv --loop=inf'
alias ma='mpv --loop=inf --no-video'
alias md='mkdir -pv'
alias mi='mediainfo'
alias mv='mv -v'
alias rm='rm -vI'
alias rmdir='rmdir -v'
alias sd='sudo shutdown now'
alias se='sudoedit'
alias spc='lsblk -o NAME,FSTYPE,FSVER,LABEL,UUID,FSSIZE,FSAVAIL,FSUSE%,MOUNTPOINTS'
# I only use stow to manage dotfiles.
alias stow='stow -t ~ --no-folding --dotfiles'
alias t='tree -F'
alias ta='tree -aF -I .git'
alias th='tree -phF'
alias tah='tree -aphF -I .git'
# The single quotes in the search patterns ensure that the line itself is not
# listed in the search results.
alias todo='git grep -I -A 2 -e T''ODO: -e F''IXME:'
alias v='vim'
alias z='zathura --fork'

# -----------------------------------------------------------------------------
function exitstatus {
  local exitcode=$?

  if [[ $exitcode == 0 ]]; then
    tput setaf 2
  else
    tput setaf 1
    [[ $exitcode != 1 ]] && echo " $exitcode"
  fi
}

# ➜
PS1=' \e[33m\w\[$(exitstatus)\] \e[1m>\e[2m \[$(tput sgr0; echo -en "\x1b[\x36 q")\]'

if [[ -n "$GUIX_ENVIRONMENT" ]]; then
  PS1=" [dev]$PS1"
fi

# -----------------------------------------------------------------------------
# [[ ${BLE_VERSION-} ]] && ble-attach

# Local Variables:
# mode: sh
# End:
